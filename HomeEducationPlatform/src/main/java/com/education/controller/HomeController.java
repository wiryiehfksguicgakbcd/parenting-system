package com.education.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.education.entity.Tnews;
import com.education.entity.Users;
import com.education.service.common.CommonService;
import com.education.service.log.LoginLogService;
import com.education.service.news.TnewsService;
import com.education.util.IPUtil;
import com.sun.org.apache.bcel.internal.generic.NEW;

@Controller
@RequestMapping("/home")
public class HomeController {
	@Autowired
	private CommonService commonService;
	@Autowired
	private  TnewsService tnewsService;
	@Autowired
	private LoginLogService logService;
	@RequestMapping("/index")
	public String index(){
		
		return "system/applicant/index.html";
	}
	@RequestMapping("/register")
	public String register(){
		
		return "system/applicant/register.html";
	}
	
	@RequestMapping("/toCreateNote")
	public String toCreateNote(){
		
		return "system/applicant/create_note.html";
	}
	@RequestMapping("/addNotice")
	public String addNotice(){
		return "system/admin/notice_add.html";
	}
	@RequestMapping(value="/checkFlag",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> checkFlag(HttpSession session){
		 Map<String, Object> map = new HashMap<String, Object>();
		 if (session.getAttribute("flag")!=null) {
			map.put("flag", session.getAttribute("flag"));
		}else{
			session.setAttribute("flag", 0);
		}
		 //是否登录标记（登录教师为1 登录 学生家长为2）
		 
		 return map;
				 
	}
	/**
	 * 前台用户登录操作
	 * @param username
	 * @param password
	 * @param session
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/loginIn",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> loginIn(Users user,HttpSession session,HttpServletRequest request){
		 Map<String, Object> ret = new HashMap<String, Object>();
		 ret.put("type", "error");
		 if(StringUtils.isEmpty(user.getName())) {
			 ret.put("message", "请输入用户名");
			 return ret;
		 }
		 if(StringUtils.isEmpty(user.getPassword())) {
			 ret.put("message", "请输入密码");
			 return ret;
		 }
		 Users users = commonService.getUsersByName(user.getName());
		 if(users == null){
			ret.put("message", "该用户未注册,请先去注册");
			return ret;
		 }else{
			 String password = users.getPassword();
			 if(password.equals(user.getPassword())) {
				 	//表示账号密码正确
				 session.setAttribute("userid", users.getId());
				 session.setAttribute("username", users.getName());
				 session.setAttribute("user", users);
				 if(users.getStatue()==2) {
					 session.setAttribute("flag", 1);
				 }else {
					 session.setAttribute("flag", 2);
				 }
				 
			 }else {
				 ret.put("message", "密码错误");
				 return ret;
			 }
			 logService.addLoginLog(users.getId(), users.getName(), IPUtil.getIpAddr(request));
		 }
		 ret.put("type", "success");
		 ret.put("message", "注册成功");
		 return ret;
				 
	}
	/**
	 * 注销
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/loginout",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> loginout(HttpSession session){
		 Map<String, Object> map = new HashMap<String, Object>();
		 session.removeAttribute("username");
		 session.removeAttribute("userid");
		 session.removeAttribute("user");
		 //是否登录标记（登录教师为1 登录 学生家长为2）
		 session.setAttribute("flag", 0);
		 map.put("type", "success");
		 return map;
				 
	}
	
	/**
	 * 用户名验证
	 * @param username
	 * @return
	 */
	@RequestMapping(value="/checkName",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> checkName(@RequestParam("username")String username){
		 Map<String, Object> map = new HashMap<String, Object>();
		 map.put("type", "error");
		 if(StringUtils.isEmpty(username)) {
			 	map.put("message", "请填写用户名");
			 	return map;
		 }
		 Users users = commonService.getUsersByName(username);
		 if(users != null){
			 //用户名可以使用
			 map.put("type", "error");
			 map.put("message", "该用户名已被注册");
			 	return map;
		 }
		 map.put("type", "success");
		 map.put("message", "用户名可以使用");
		 return map;
				 
	}
	/**
	 * 邮箱验证
	 * @param email
	 * @return
	 */
	@RequestMapping(value="/checkEmail",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> checkEmail(@RequestParam("email")String email){
		 Map<String, Object> map = new HashMap<String, Object>();
		 map.put("type", "error");
		 if(StringUtils.isEmpty(email)) {
			 	map.put("message", "请填写邮箱");
			 	return map;
		 }
		 Users users = commonService.getUsersByEmail(email);
		 if(users != null){
			 //用户名可以使用
			 map.put("type", "error");
			 map.put("message", "该邮箱已被注册");
			 	return map;
		 }
		 map.put("type", "success");
		 map.put("message", "该邮箱可以使用");
		 return map;
				 
	}
	/**
	 * 注册
	 * @param username
	 * @param password
	 * @param email
	 * @param status
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/registerUser",method=RequestMethod.POST)
	public Map<String,Object> registerUser(Users users,HttpSession session){
			Map<String, Object> ret = new HashMap<>();
			ret.put("type", "error");
			if(StringUtils.isEmpty(users.getName())) {
				ret.put("message", "请填写用户名");
				return ret;
			}
			if(StringUtils.isEmpty(users.getEmail())) {
				ret.put("message", "请填写邮箱");
				return ret;
			}
			if(StringUtils.isEmpty(users.getPassword())) {
				ret.put("message", "请填写密码");
				return ret;
			}
		users.setFlag(1);
		users.setCreatetime(new Date());
		commonService.addUsers(users);
		session.setAttribute("registername", users.getName());
		ret.put("type", "success");
		ret.put("message", "注册成功");
		return ret;
	}
	@RequestMapping(value="/listTnews",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> listNotices(){
		Map<String, Object> map = new HashMap<String, Object>();
		List<Tnews> news = tnewsService.findNews();
		map.put("news", news);
		System.out.println(map.toString());
		return map;
	}
	@RequestMapping(value="/getTnewsById",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getTnewsById(@RequestParam("id")int id){
		Map<String, Object> map = new HashMap<String, Object>();
		tnewsService.updateClickNum(id);
		Tnews tnews = tnewsService.getNewInfoByID(id);
		map.put("title", tnews.getTitle());
		map.put("content", tnews.getContent());
		map.put("clicknum", tnews.getClicknum());
		map.put("createtime", tnews.getCreatetime());
		System.out.println(map.toString());
		return map;
	}
	
	//找回密码操作
	/**
	 * 找回密码操作
	 * @param user
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/forgetPassword",method = RequestMethod.POST)
	public Map<String,Object> forgetPassword(Users user){
		Map<String, Object> ret = new HashMap<>();
		ret.put("type", "error");
		 if(StringUtils.isEmpty(user.getName())) {
			 ret.put("message", "请输入用户名");
			 return ret;
		 }
		 if(StringUtils.isEmpty(user.getPassword())) {
			 ret.put("message", "请输入密码");
			 return ret;
		 }
		 if(StringUtils.isEmpty(user.getEmail())) {
			 ret.put("message", "请输入邮箱");
			 return ret;
		 }
		 Users findUserByNameAndEmail = commonService.findUserByNameAndEmail(user);
		 if(findUserByNameAndEmail==null) {
			 ret.put("message", "用户名邮箱错误");
			 return ret;
		 }
		 findUserByNameAndEmail.setPassword(user.getPassword());
		 commonService.updateUsers(findUserByNameAndEmail);
		 ret.put("type", "success");
		 ret.put("message", "找回成功");
		return  ret;
	}
	
}
